import React, { useEffect } from 'react'
import { useParams } from 'react-router-dom'


import { DetailSkelton } from './DetailSkelton';
import { useFetch } from '../../hooks';
import { DetailMovie } from './DetailMovie';

export const MovieDetailedPage = () => {

  const params = useParams();
  const { data: movie, error , setUrl, isLoading}= useFetch();

  useEffect(() => {
    const movieId = params.id;
    const URL = `${process.env.REACT_APP_API_URL}movie/${movieId}?api_key=${process.env.REACT_APP_API_KEY}`;
    setUrl(URL);
  });

  return (
    <main>
      { isLoading && <DetailSkelton/>}
      { movie && <DetailMovie movie = {movie} /> }
    </main>
  )
}
