import React, { useEffect } from 'react'
import { MovieCard } from '../components/MovieCard'
import useFetch from '../hooks/useFetch'
import { MovieCardSkeleton } from '../components/MovieCardSkelton';
import { render } from '@testing-library/react';
import { useDynamicTitle } from '../hooks/useDynamicTitle';

export const MoviePage = ({apiPath, title}) => {
  const BASE_API = process.env.REACT_APP_API_URL;
  const { data: movies, error, isLoading, setUrl } = useFetch();

  useDynamicTitle(title);

  useEffect(() => {
    setUrl(`${BASE_API}${apiPath}?api_key=${process.env.REACT_APP_API_KEY}`);
  }, [apiPath])

  function renderSkeltons(count){
    const skeltons = [];
    for(let i=1; i<count; i++){
      skeltons.push(<MovieCardSkeleton key ={i}/>);
    }
    return skeltons;
  }
  return (
    <main>
      <div className='flex flex-wrap justify-start'>
        {
          isLoading && renderSkeltons(6)
        }
       {
         !isLoading && movies && movies.results.map(movie => <MovieCard movie = {movie} key = { movie.id }/>)
       }
      </div>
    </main>
  )
}

// movies && movies.results for null.