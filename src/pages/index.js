export { MovieDetailedPage } from "./movieDetails/MovieDetailedPage";
export { MoviePage } from "./MoviePage";
export {PageNotFound} from "./PageNotFound";
export { DetailMovie } from "./movieDetails/DetailMovie";
export { DetailSkelton } from "./movieDetails/DetailSkelton";