import React from 'react'
import { AppRoutes, Footer, Header } from './components'
import { MovieDetailedPage } from './pages'

 function App() {
  return (
    <div className='dark:bg-gray-800'>
      <Header/>
      <AppRoutes/>
      <Footer/>

      
    </div>
  )
}
export default App;