export { Footer } from "./Footer";
export { Header } from "./Header";
export { AppRoutes } from "./AppRoutes";
export { MovieCard} from "./MovieCard";