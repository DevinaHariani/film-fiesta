import React from 'react'

export const Footer = () => {
  return (
  <footer className="fixed bottom-0 left-0 z-20 w-full p-4 bg-white border-t border-gray-200 shadow p-6 dark:bg-slate-900 dark:border-slate-50">
    <p className="text-sm text-gray-500 sm:text-center dark:text-gray-400">© { new Date().getFullYear()} 
   
    
    <a href="https://devina-filmfiesta.netlify.app/" className="hover:text-primary:800"> FilmFiesta&trade;</a>. All Rights Reserved.
    </p>
    </footer>

  )
  
}
