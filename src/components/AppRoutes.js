import React from 'react'
import { Route, Routes } from 'react-router-dom'
import { MovieDetailedPage, MoviePage, PageNotFound } from '../pages'
import { SearchPage } from '../pages/SearchPage'

export const AppRoutes = () => {
  return (
   
        <>
        <Routes>
            <Route path = "/" element = { <MoviePage apiPath= {'movie/now_playing'} title ="Now playing | FilmFiesta" />}/>
            <Route path = "/movies/top-rated" element = { <MoviePage apiPath= {'movie/top_rated'}  title ="Top Rated | FilmFiesta" />}/>
            <Route path = "/movies/popular" element = { <MoviePage apiPath= {'movie/popular'}  title ="Popular | FilmFiesta" />}/>
            <Route path = "/movies/upcoming" element = { <MoviePage apiPath= {'movie/upcoming'}  title ="Upcoming | FilmFiesta" />}/>
            <Route path = "/movies/search" element = { <SearchPage apiPath= {'search/movie'}/>}/>
            <Route path = "/movies/:id" element = { <MovieDetailedPage/>}/>
            <Route path = "*" element = { <PageNotFound/>}/>
            
        </Routes>
        
        </>
   
  )
}
