import React from 'react'
import { Link } from 'react-router-dom'


export const MovieCard = ({ movie }) => {

    const BASE_IMAGE_URL = "https://image.tmdb.org/t/p/original"; 
    const imageURL = movie.poster_path ? `${BASE_IMAGE_URL}${movie.poster_path}` : "https://placehold.co/382x573?text=No%20Image";
    
  return (
<div class="max-w-sm bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700 m-2">
    <Link to={`/movies/${movie.id}`}>
        <img class="rounded-t-lg" src={imageURL} alt="" />
    </Link>
    <div class="p-5">
    <Link to={`/movies/${movie.id}`}>
            <h5 class="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white hover:text-primary-800 dark:hover:text-primary-800">{movie.title}</h5>
        </Link>
        
        <p class="mb-3 font-normal text-gray-700 dark:text-gray-400">{ movie.overview.substring(0,150)}..</p>
        <Link to={`/movies/${movie.id}`} class="inline-flex items-center px-3 py-2 text-sm font-medium text-center text-white bg-primary-800 rounded-lg hover:bg-primary-1000 focus:outline-none">
            Read more
             <svg class="rtl:rotate-180 w-3.5 h-3.5 ms-2" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 14 10">
                <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M1 5h12m0 0L9 1m4 4L9 9"/>
            </svg>
        </Link>
    </div>
</div>

  )
}
